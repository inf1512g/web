function salut(nume="Moraru", prenume="Ion") {
    return `Bună ziua ${nume} ${prenume}.`
}

console.log(salut()); // Bună ziua Moraru Ion.

console.log(salut('Retco', 'Boris')); // Bună ziua Retco Boris.
