const inventar = [
    { tipul: "masina", valoare: 5000 },
    { tipul: "masina", valoare: 650 },
    { tipul: "jucarie", valoare: 10 },
    { tipul: "laptop", valoare: 1200 },
    { tipul: "telefon", valoare: 69 }
]

const reducer = (acumulator, item) => acumulator + item.valoare;

const total = inventar.reduce(reducer, 0)

console.log(total)