console.log("///3");
const calatorie = {
    marca: "Tesla",
    model: "Roadster",
    an: 2018,
    locatie: "La serviciu",
    plecare: function ( loc ) { loc ? this.locatie=loc : this.locatie="Acasa"}
}

calatorie.plecare("Metro")
console.log(calatorie.locatie) // "Metro"


calatorie.plecare()
console.log(calatorie.locatie) // "Acasă"
