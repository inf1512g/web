const data = () => [1, 2, 3, 4];

let [a, b, , d] = data();

console.log(a, b, d); // 1 2 4

// Realizează interschimbarea astfel încât 
// valorile dintre `a` și `d` să fie inversate
// folosind destructurizarea si o singura linie de cod

console.log(a, d); // 1, 4

[a, b, , d]=[d, b, , a];

console.log(a, d); // 4, 1
