console.log("///7");
const inventar = [
    {tipul:    "masina", valoare: 5000},
    {tipul:    "masina", valoare:  650},
    {tipul:   "jucarie", valoare:   10},
    {tipul:    "laptop", valoare: 1200},
    {tipul:   "telefon", valoare:   69}
]

const reducer = function(acumulator, i) {
    return acumulator + i.valoare;
}

const total = inventar.reduce(reducer, 0)
console.log(total)
// => 6969

