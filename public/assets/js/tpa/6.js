const grup  = "INF1512"
const elevi = [ {nume: "Ion",    rol: "student"},
                {nume: "Andrei", rol: "boss"   },
                {nume: "Marcel", rol: "student"},
                {nume: "Dimon",  rol: "student"} 
            ]
let message = `În grupul ${grup} sunt ${elevi.length} elevi. Aceștia sunt ${elevi.map(e => ` ${e.nume}`)}`   
console.log(message)