const inventar = [
    {tipul:    "masina", valoare: 5000},
    {tipul:    "masina", valoare:  650},
    {tipul:   "jucarie", valoare:   10},
    {tipul:    "laptop", valoare: 1200},
    {tipul:   "telefon", valoare:   69}
]
const reducer = function(acumulator, item) {
    return acumulator + item.valoare;
}
const total = inventar.reduce(reducer, 0)
console.log(total)
// Btw if you read this, you should not it's 6929 not 6969 (medium.com)