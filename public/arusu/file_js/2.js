const calatorie = {
    marca: "Tesla",
    model: "Roadster",
    an: 2018,
    locatie: "La serviciu",
    plecare: function ( loc ) {
        this.locatie = loc ? loc : "Acasă"
        // Dacă `loc` există atribuim `loc` (this.locatie = loc)
        // în caz contrat atribuim "Acasă"  (this.locatie = "Acasă")
    }
}