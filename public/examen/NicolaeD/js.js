function test() {

            const form = document.forms[0];

            const { nume, prenume } = form;
            
            if (!nume.value || !prenume.value)
                alert('Introduceti numele/prenumele');
            else {
                
                document.cookie = JSON.stringify({
                    "nume" : nume.value,
                    "prenume": prenume.value
                })

                alert(`Salut, ${nume.value} ${prenume.value}!`)
                window.open('test.html', '_blank');
            }
            
            return false;
        }