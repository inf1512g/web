const [ q1, q2, q3, q4 ] = document.querySelectorAll('p');
        
        const { a1, a2, a3, a4 } = document.forms[0];

        const answers = [];
        let correctAnswers = 0;

        const form = document.forms[0]
        const p = document.createElement('p');
        const question = document.getElementById('q1');
        form.insertBefore(p, question);
        
        let s = 60;
        setInterval(() => {
            if (s >= 0)
                p.innerText = `Timp ramas: ${s--} secunde`;
        }, 1000);


        a1.forEach((element, i) => {
            element.addEventListener('input', () => {
                if (i == 1){
                    correctAnswers++;
			 i.style.color = "green";}
                else
                    alert('Raspuns gresit')
                a1[0].disabled = true;
                a1[1].disabled = true;
                a1[2].disabled = true;
            })
        });

        a2.addEventListener('focusout', () => {
            if (a2.value == 'Spirit'){
                correctAnswers++;
				a2.style.background = "green";}
				
            else
                alert('Raspuns gresit')
            a2.disabled = true;
        })

        a3.addEventListener('input', () => {
            if (a3.value == 'Null1')
                correctAnswers++;
            else
                alert('Raspuns gresit')
            a3.disabled = true;
        })

        a4.addEventListener('input', () => {
            if (a4.value == 'NULL3')
                correctAnswers++;
            else
                alert('Raspuns gresit')
            a4.disabled = true;
        })

        function trimite() {

            if (a1.value && a2.value && a3.value && a4.value) {
                p.style.display = 'none';
                form.button.disabled = true;
                answers.push([
                    {
                        [q1.innerText]: a1['value']
                    },
                    {
                        [q2.innerText]: a2['value']
                    },
                    {
                        [q3.innerText]: a3['value']
                    },
                    {
                        [q4.innerText]: a4['value']
                    }
                ]);

                afiseaza();

            } else {
                alert('Alegeti raspunsurile')
            }         

            return false;
        }

        function afiseaza() {
            
            const cookies = document.cookie;

            const info = JSON.parse(cookies);
        
            var w = window.open("about:blank","new Window","width=400, height=400, top=400,left=400");
            w.document.write(
                answers.map((element) => {
                    return JSON.stringify(element);
                })
            )
            w.document.write(`<br><h1>${info.nume} ${info.prenume}</h1>`)
            w.document.write(`<br><h4>Raspunsuri corecte: ${correctAnswers}</h1>`)

        }